var fs = require("fs");
var ytdl = require("ytdl-core");
var ffmpeg = require("fluent-ffmpeg");
var mongo = require("../scripts/mongo.js");
var path = require("path");
var ObjectId = require('mongodb').ObjectID;

exports.getVideoInfo = function (link) {
    var url = link.data.url;

    if (!link.data.url) {
        link.res.writeHead(400);
        link.res.end("Why the f**k didn't you enter a valid url?");
        return;
    }

    ytdl.getInfo(link.data.url, function (err, info) {

        // handle error
        if (err) {
            link.res.writeHead(400);
            link.res.end("Why the f**k didn't you enter a valid url?");
            return;
        }

        link.res.writeHead(200);
        link.res.end(JSON.stringify(info));
    });
};

exports.process = function (link) {
    
    if (!link.data.url) {
        link.res.writeHead(400);
        link.res.end("Why the f**k didn't you enter a valid url?");
        return;
    }

    ytdl.getInfo(link.data.url, function (err, info) {

        // handle error
        if (err) {
            link.res.writeHead(400);
            link.res.end("Why the f**k didn't you enter a valid url?");
            return;
        }

        // connect to db collection
        mongo.getCollection('youtube_videos', function (err, col) {

            // handle error
            if (err) {
                link.res.writeHead(500);
                link.res.end(JSON.stringify(err));
                return;
            }

            var videoObj = {
                title: info.title,
                url: link.data.url
            };

            col.insert(videoObj, function (err, records) {

                // handle error
                if (err) {
                    link.res.writeHead(500);
                    link.res.end(JSON.stringify(err));
                    return;
                }

                var stream = ytdl(link.data.url);
                var proc = new ffmpeg({source: stream});

                var musicDir = path.join(__dirname, '../..', '/videos/');
                var filename = musicDir + info.title + ".mp3";
                var id = records[0]._id;
                var slug = musicDir + id + ".mp3";

                proc.setFfmpegPath("/usr/local/Cellar/ffmpeg/2.5/bin/ffmpeg");
                proc.saveToFile(slug);
                proc.on('end', function () {
                    link.res.writeHead(200);
                    link.res.end(id + "");
                });
            });
        });
    });
};

exports.download = function (link) {

    var id = link.data.id;

    // connect to db collection
    mongo.getCollection('youtube_videos', function (err, col) {

        // handle error
        if (err) {
            link.res.writeHead(500);
            link.res.end(JSON.stringify(err));
            return;
        }

        col.find({_id: ObjectId(id)}).toArray(function (err, docs) {

            // handle error
            if (err) {
                link.res.writeHead(500);
                link.res.end(JSON.stringify(err));
                return;
            }
            
            var musicDir = path.join(__dirname, '../..', '/videos/');

            link.res.download(musicDir + id + ".mp3", docs[0].title + ".mp3", function (err) {
                
                // handle error
                if (err) {
                    link.res.writeHead(500);
                    link.res.end(JSON.stringify(err));
                    return
                }
                
                // delete file from server
                fs.unlink(musicDir + id + ".mp3");
            });
        });
    });
};
