var ObjectId = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient;

// db cache
var dbCache = {}
var collectionCache = {}

// private function for fetching db
function getDb(dbName, callback) {

	// check if db exists in chache
	if (dbCache[dbName]) {
		return callback(null, dbCache[dbName]);
	}

	// if db does not exist in cache, cache it
	MongoClient.connect('mongodb://127.0.0.1:27017/music', function (err, db) {

        // handle error
        if (err) { return callback(err); }

        // cache the db
        dbCache[dbName] = db;

        // return the db
        return callback(null, db);
    });
}

// public function for getting a collection
exports.getCollection = function (options, callback) {

	// convert string to object
    if (typeof options === 'string') {
        options = {
            name: options
        }
    }
    
    // validate
    if (!options.name) {
    	return callback('Collection name required!');
    }

    // add default db
    options.db = options.db || 'music';

    // check the cache for collection
    if (collectionCache[options.db] && collectionCache[options.db][options.name]) {
    	return callback(null, collectionCache[options.db][options.name]);
    }

    // get the collection
    getDb(options.db, function (err, db) {

    	if (err) { return callback(err); }

    	db.collection(options.name, function (err, col) {

    		if (err) { return callback(err); }

    		// cache the col
    		collectionCache[options.db] = collectionCache[options.db] || {};
    		collectionCache[options.db][options.name] = col;

    		// return the col
    		return callback (null, col);
    	});
    });
}