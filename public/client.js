$(document).ready(function () {

    var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );

    if (iOS) {
        alert("You have an iOS device and they don't support web downloads, so this application won't work for you. Sorry!");
    }

    $("input[name=url]").on('input', function () {
        makeAjaxRequest({
            operation: "/@/downloader/getVideoInfo",
            data: { url: $("input[name=url]").val() }
        }, function (err, res) {
            if (err) {
                $(".videoTitle").html("");
                $(".thumbnail").hide();
                return;
            }
            res = JSON.parse(res);
            $(".videoTitle").html(res.title);
            $(".thumbnail").attr("src", res.thumbnail_url).show();
        });
    });

	$(".downloadForm").submit(function (e) {
        $(".loaderContainer").fadeIn(500);
        $(".submit").attr("disabled", "true");
		makeAjaxRequest({
			operation: "/@/downloader/process",
			data: { url: $("input[name=url]").val() }
		}, function (err, res) {

			if (err) {
				alert(err);
                $(".loaderContainer").fadeOut(500);
                $("input[name=url]").val("");
                $(".submit").removeAttr("disabled");
				return;
			}

            if (res) {
    			// do stuff when everything is ok
                $(".loaderContainer").fadeOut(500);
    			$("input[name=url]").val("");
                $(".submit").removeAttr("disabled");
                $(".videoTitle").html("");
                $(".thumbnail").hide();
                $("#downloadIframe").attr("src", "/@/downloader/download?id=" + res);
            }
		});

		e.preventDefault();
		return false;
	});
});

function makeAjaxRequest (ajaxObj, callback) {
    $.ajax({
        url: ajaxObj.operation,
        type: "POST",
        data: ajaxObj.data,
        crossDomain: true,
        error: function(jqXHR, exception) {
            if (jqXHR.status === 0) {
                console.log(exception);
                callback('Not connect. Verify Network.' + jqXHR.responseText);
            } else if (jqXHR.status == 404) {
                callback('Requested page not found. [404].' + jqXHR.responseText);
            } else if (jqXHR.status == 500) {
                callback('Internal Server Error [500].' + jqXHR.responseText);
            } else if (exception === 'parsererror') {
                callback('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                callback('Time out error.');
            } else if (exception === 'abort') {
                callback('Ajax request aborted.');
            } else {
                callback('Uncaught Error.\n' + jqXHR.responseText);
            }
        },
        success: function (data) {
            callback(null, data);
        }
    });
}